# rice

My configs, scripts and dotfiles

I try to keep these configs as **short** and **simple** as possible.

* my script for dwm (https://dwm.suckless.org/status_monitor/)
* vimrc
* dunstrc
* .tmux.conf